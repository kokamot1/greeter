package edu.westga.cs6241.greeter.model;

/**
 * Model class for a greeting
 * @author kenji okamoto
 * @version 20150825
 *
 */
public class Greeter {

    private String greeting;

    /**
     * Create a new Greeting instance
     * @param greeting The greeting to use
     * Precondition: greeting is not null or an empty string
     * Postcondition: getGreeting().equals(greeting)
     */
    public Greeter(String greeting) {
        if (greeting == null) {
            throw new IllegalArgumentException("greeting cannot be null");
        } else {
            this.greeting = greeting;
        }
    }

    /**
     * Get the greeting
     * @return The greeting this object has
     */
    public String getGreeting() {
        return this.greeting;
    }
}
