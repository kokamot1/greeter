package edu.westga.cs6241.greeter.model.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6241.greeter.model.Greeter;

/**
 * Tests for the Greeter class
 * @author kenji okamoto
 * @version 20150825
 *
 */
public class GreeterConstructionTest {

    /**
     * No-op constructor,
     */
    public GreeterConstructionTest() {
        // no-op
    }


    /**
     * Test method for
     * {@link edu.westga.cs6241.model.Greeter#Greeter(java.lang.string)}
     */
    @Test
    public void shouldGet1LetterGreeting() {
        Greeter testGreeter = new Greeter("A");
        assertEquals("A", testGreeter.getGreeting());
    }

}
